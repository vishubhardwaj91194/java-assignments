import java.util.*;
import java.io.*;

public class LinkedTest {
	
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		LinkedList<Integer> linked = new LinkedList<Integer>();
		System.out.print("Enter no of elemnts: ");
		int n = Integer.parseInt(br.readLine());
		
		int a, i, j, b;
		
		for(i=0;i<n;i++){
			a = Integer.parseInt(br.readLine());
			linked.add(a);
		}
		System.out.println("Linked list before sorting: "+linked);
		
		for(i=0;i<n-1;i++) {
			for(j=0;j<n-i-1;j++) {
				a = linked.get(j);
				b = linked.get(j+1);
				
				if(a > b) {
					// swap a,b ie j, j+1
					linked.set(j, b);
					linked.set(j+1, a);
				}
			}
		}
		System.out.println("Linked list after sorting: "+linked);
		
		LinkedList<Integer> newLinked = new LinkedList<Integer>();
		for(i=0;i<linked.size();i++){
			newLinked.add(linked.removeFirst());
		}
		System.out.println("2 linked list after breaking");
		System.out.println("First: "+newLinked);
		System.out.println("Second: "+linked);
		
		
	}
}


