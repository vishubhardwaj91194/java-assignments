# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Java assignments Daffodil
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install java jdk and jre
* Configure path variable
* Dependencies none
* Database configuration none
* How to run tests java file-name-goes-here.java
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Vishu Bhardwaj
* Daffodilsw


**# Assignments: #**

Assignment 1: Take details of employee. and save into the text file in form of Json. Then read the file and update his salary 
Assignment 2:  Create one thread A, It will create one more thread B, the B will incumbent a number upto 500, A should wait till B finish. Once B finish A should resume and show msg say “B is completed” Then A should be completed.
Assignment 3: Create a timer application. User should enter time in form of HH:mm:ss. You need to show decremental timer till 0.
Assignment 4: Create a linked list of integers. Sort it. Then break it into 2 equal halfs.
Assignment 5: Create a priority queue of Tasks, Every task is said to be complete when it count a number till 10 by waiting 1 sec. Add 10 tasks to queue of different priority.
Assignment 6: Extend the above assignment to create 2 more threads, One will add new tasks to queue and other will keep watching the queue. If queue has new task then it will pick task according to priority and then excite it.

You can contact to me if you feel difficulty in understanding the assignments above.