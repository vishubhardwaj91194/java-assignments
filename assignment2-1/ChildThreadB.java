import java.util.concurrent.locks.*;

public class ChildThreadB implements Runnable {
	String threadName;
	private final Lock queueLock = new ReentrantLock();
	
	
	ChildThreadB() {
		this.threadName = "Anonymous";
	}
	ChildThreadB(String name) {
		this.threadName = name;
	}
	
	@Override
	public void run() {
		System.out.println("Starting B");
		
		
		queueLock.lock();
		//synchronized(this){
	      try
	      {
	    	  ProgramLogic pl = new ProgramLogic(12);
              pl.count();
	         //Thread.sleep(duration);
	      } catch (InterruptedException e)
	      {
	         e.printStackTrace();
	      } finally
	      {
	    	  //System.out.println("Ending A");
	         queueLock.unlock();
	      }
	      CustomLock.notifyCustom();
	      //notify();
		//}
        System.out.println("Ending B");
	}
}