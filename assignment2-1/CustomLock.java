public class CustomLock {
	
	public static boolean canRelease = false;
	public static boolean stopChild = false;
	
	public static boolean waitCustom() {
		return canRelease;
	}

	
	public static void  notifyCustom() {
		canRelease = true;
	}
}