import java.util.*;

public class RemindTimer extends TimerTask {
	@Override
    public void run() {
        if(!CustomLock.waitCustom()) {
        	CustomLock.notifyCustom();
        	CustomLock.stopChild = true;
        }
        //timer.cancel(); //Not necessary because we call System.exit
        //System.exit(0); //Stops the AWT thread (and everything else)
    }
}