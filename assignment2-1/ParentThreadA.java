import java.util.Timer;
import java.util.concurrent.locks.*;
public class ParentThreadA implements Runnable {
	String threadName;
	private final Lock queueLock = new ReentrantLock();
	
	ParentThreadA() {
		this.threadName = "Anonymous";
	}
	ParentThreadA(String name) {
		this.threadName = name;
	}
	
	@Override
	public void run() {
		System.out.println("Starting A");
		
		queueLock.lock();
		ChildThreadB threadObjectB = new ChildThreadB("Child B");
		Thread thread2 = new Thread(threadObjectB);
		thread2.start();
		//CustomLock l = new CustomLock();
		//synchronized(l){
	      try
	      {
	    	  System.out.println("Waiting for B.");
	    	  
	    	  Timer timer = new Timer();
	    	  timer.schedule(new RemindTimer(), 8000);
              
	    	  
	    	  while(!CustomLock.waitCustom()) 
            	  	Thread.sleep(500);
	    	  if(CustomLock.stopChild){
	    		  thread2.interrupt();
	    		  timer.cancel();
	    	  }
	      } catch (Exception e)
	      {
	         e.printStackTrace();
	      } finally
	      {
	    	  System.out.println("Ending A");
	         queueLock.unlock();
	      }
	      //System.out.println("Ending A");
		//}
	}
}