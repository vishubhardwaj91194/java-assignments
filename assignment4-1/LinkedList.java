
class LinkedListEmptyException extends RuntimeException{
       public LinkedListEmptyException(){
         super();
       }
      
     public LinkedListEmptyException(String message){
         super(message);
       }  
}
 
/**
 *Node class, which holds data and contains next which points to next Node.
 */
class Node<T> {
    public T data; // data in Node.
    public Node<T> next; // points to next Node in list.
 
    /**
     * Constructor
     */
    public Node(T data){
           this.data = data;
    }
 
    /**
     * Display Node's data
     */
    public void displayNode() {
           System.out.print( data + " ");
    }
}
 
 
/**
 * generic Single LinkedList class (Generic implementation)
 */
public class LinkedList<T> {
    private Node<T> first; // ref to first link on list
 
    /**
     * generic Single LinkedList constructor
     */
    public LinkedList(){
           first = null;
    }
 
    /**
     * Insert New Node at first position of generic Single LinkedList 
     */
    public void insertFirst(T data) {
           Node<T> newNode = new Node<T>(data);  //Creation of New Node.
           newNode.next = first;   //newLink ---> old first
           first = newNode;  //first ---> newNode
    }
 
    public void insertLast(T data) {
    	Node<T> newNode = new Node<T>(data);
    	newNode.next = null;
    	
    	//Node<T> xNode = new Node<T>(data);
    	//xNode.next = first;
    	
    	// 1 -> null
    	
    	if(first == null){
    		first = newNode;
    	} else {
    		Node<T> temp = first;
    		Node<T> tempNew = null;
        	while(temp != null) {
        		tempNew = temp;
        		temp = temp.next;
        	}
    		tempNew.next = newNode;
    	}
    	
    	
    	
    	
    }
    
    public T get(int i) {
    	if(i==0) {
    		return first.data;
    	}else {
    		int j = 0;
    		Node<T> temp = first;
    		Node<T> tempNew = null;
    		while(j!=i) {
        		tempNew = temp;
        		temp = temp.next;
        		j++;
        	}
    		return temp.data;
    	}
    }
    
    public void set(int i, T data) {
    	int j = 0;
    	Node<T> temp = first;
		Node<T> tempNew = null;
    	while(j!=i) {
    		tempNew = temp;
    		temp = temp.next;
    		j++;
    	}
    	temp.data = data;
    }
    
    
    public void sortCustom(int n) {
    	Node<T> steps, prev, curr, next, temp;
    	for(int i=0; i<n-1; i++){
    		curr=first;
    		for(int j=0; j<n-i-1;j++){
    			next=curr.next;
    			if(Integer.parseInt(curr.data.toString()) > Integer.parseInt(next.data.toString())){
    				if(first == curr){
    					temp = next.next;
    					next.next = curr;
    					curr.next = temp;
    					first = next;
    				}
    				else{
    					prev = first;
    					while(prev.next!=curr){
    						prev=prev.next;
    					}
    					temp = next.next;
    					next.next = curr;
    					curr.next = temp;
    					prev.next = next;
    				}	
    			}
    		}
    	}
    }
    
    public LinkedList<Integer> breakCustom(int n){
    	Node<T> ptr,ptr1;
    	LinkedList<Integer> newNode = new LinkedList<Integer>();
    	newNode.first = (Node<Integer>)first;
    	ptr = first;
    	ptr1 = (Node<T>)newNode.first;
    	for(int i=0;i<n/2;i++){
			ptr = ptr.next;
			if(i!=((n/2)-1)){
				ptr1 = ptr1.next;
			}
		}
    	ptr1.next = null;
    	first = ptr;
    	return newNode;
    }
    
    
    /**
     * Deletes first Node of generic Single LinkedList 
     */
    public T deleteFirst()
    {
           if(first==null){  //means LinkedList in empty, throw exception.              
                  throw new LinkedListEmptyException("LinkedList doesn't contain any Nodes.");
           }
           Node<T> tempNode = first; // save reference to first Node in tempNode- so that we could return saved reference.
           first = first.next; // delete first Node (make first point to second node)
           return tempNode.data; // return tempNode (i.e. deleted Node)
    }
    
           
    /**
     * Display generic Single LinkedList 
     */
    public void displayLinkedList() {
           System.out.print("Displaying LinkedList [first--->last]: ");
           Node<T> tempDisplay = first; // start at the beginning of linkedList
           while (tempDisplay != null){ // Executes until we don't find end of list.
                  tempDisplay.displayNode();
                  tempDisplay = tempDisplay.next; // move to next Node
           }
           System.out.println();
           
    }
 
}