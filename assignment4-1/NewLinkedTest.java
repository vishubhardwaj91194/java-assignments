//import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class NewLinkedTest{
	
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		LinkedList<Integer> linked = new LinkedList<Integer>();
		System.out.print("Enter no of elemnts: ");
		int n = Integer.parseInt(br.readLine());
		
		int a, i, j, b;
		
		for(i=0;i<n;i++){
			a = Integer.parseInt(br.readLine());
			linked.insertLast(a);
		}
		System.out.println("Linked list before sorting: ");
		linked.displayLinkedList();
		
		linked.sortCustom(n);
		
		
		System.out.println("Linked list after sorting: ");
		linked.displayLinkedList();
		
		LinkedList<Integer> newLinked = new LinkedList<Integer>();
		newLinked = linked.breakCustom(n);
		
		
		/*for(i=0;i<n/2;i++){
			newLinked.insertLast(linked.deleteFirst());
		}*/
		System.out.println("2 linked list after breaking");
		System.out.println("First: ");
		newLinked.displayLinkedList();
		System.out.println("Second: ");
		linked.displayLinkedList();
		
	}
	
	
}