public class MultiThreadTest {
	public static void main(String args[]) {
		
		System.out.println("Starting Program.");
		
		ParentThreadA threadObjectA = new ParentThreadA("Parent A");
		Thread thread1 = new Thread(threadObjectA);
		thread1.start();
		
		System.out.println("Ending Program.");
	}
}