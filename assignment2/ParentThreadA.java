public class ParentThreadA implements Runnable {
	String threadName;
	
	ParentThreadA() {
		this.threadName = "Anonymous";
	}
	ParentThreadA(String name) {
		this.threadName = name;
	}
	
	@Override
	public void run() {
		System.out.println("Starting A");
		
		ChildThreadB threadObjectB = new ChildThreadB("Child B");
		Thread thread2 = new Thread(threadObjectB);
		thread2.start();
 
        synchronized(thread2){    
            try{
                
                System.out.println("Waiting for B.");
                thread2.wait();
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("Ending A");
		
		
	}
}