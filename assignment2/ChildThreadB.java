public class ChildThreadB implements Runnable {
	String threadName;
	
	ChildThreadB() {
		this.threadName = "Anonymous";
	}
	ChildThreadB(String name) {
		this.threadName = name;
	}
	
	@Override
	public void run() {
		System.out.println("Starting B");
        synchronized(this){
            try{
            	ProgramLogic pl = new ProgramLogic(10);
                pl.count();
            }catch(InterruptedException e){
            	System.out.println(e);
            }
        	
            notify();
            
        }
        System.out.println("Ending B");
	}
}