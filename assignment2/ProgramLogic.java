public class ProgramLogic {
	int countTill;
	
	ProgramLogic() {
		this.countTill = 500;
	}
	ProgramLogic(int number) {
		this.countTill = number;
	}
	
	public void count() throws InterruptedException {
		for(int i=0;i<this.countTill;i++){
			System.out.println("\nVariable i is now ("+i+1+") ");
			Thread.sleep(500);
		}
	}
}