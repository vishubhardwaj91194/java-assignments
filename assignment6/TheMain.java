import java.io.*;
import java.util.*;

public class TheMain{

     public static void main(String []args) throws IOException{

	PriorityQueue<Task> prq = new PriorityQueue<Task>(new Comparator<Task>(){
		public int compare(Task l1, Task l2){
		    return l1.priority - l2.priority;
		}
    	});
	
	Add a = new Add(prq);
	Thread ta = new Thread(a);
	Thread tp = new Thread(new Poll(prq,a));
	tp.start();
	ta.start();

     }
}

