import java.util.*;

public class Poll implements Runnable {
	PriorityQueue<Task> prq;
	Add a;
	Poll(PriorityQueue<Task> pr, Add a){
		this.prq = pr;
		this.a =a;
	}

	public void run() {
		
		System.out.println("Poll starting.");
		boolean ch;
		Thread t;
		Task head;
		do{
			synchronized(this.a){
				try{a.wait();}catch(Exception e){}
				head = prq.poll();
				t = new Thread(head);
				System.out.println("Starting task-"+head.priority);
				t.start();
				
				//try{a.wait();}catch(Exception e){}
			}
		}while(true);
		//System.out.println("Poll stopping.");
	}
}
