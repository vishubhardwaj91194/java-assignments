public class Timer {
	int hh;
	int mm;
	int ss;
	
	Timer() {
		this.hh=0;
		this.mm=0;
		this.ss=0;
	}
	Timer(int hh, int mm, int ss) {
		int x;
		
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
		
		if(this.ss >= 60) {
			x = this.ss/60;
			this.mm += x;
			this.ss %= 60;
		}
		if(this.mm >= 60) {
			x = this.mm/60;
			this.hh += x;
			this.mm %= 60;
		}
	}
	
	public String getCurrentTime() {
		return String.format("Timer set for %02d:%02d:%02d", this.hh, this.mm, this.ss);
	}
	
	public void runTimer() {
		TimerRunner tr = new TimerRunner(this.hh, this.mm, this.ss);
		Thread t = new Thread(tr);
		t.start();
	}
	
}