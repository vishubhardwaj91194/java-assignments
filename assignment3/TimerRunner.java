public class TimerRunner implements Runnable {
	int hh;
	int mm;
	int ss;
	
	TimerRunner(int hh, int mm, int ss) {
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
	}
	
	public int returnSeconds() {
		return ((this.hh*60 + this.mm)*60 + this.ss);
	}
	
	public void reduceASecond() {
		if(this.ss == 0) {
			if(this.mm == 0) {
				if(this.hh == 0) {
					// do nothing
				} else {
					this.hh--;
					this.mm=59;
					this.ss=59;
				}
			} else {
				this.mm--;
				this.ss=59;
			}
		}else {
			this.ss--;
		}
	}
	
	public void run() {
		while(this.returnSeconds() != 0) {
			System.out.println(String.format("Timer ==> %02d:%02d:%02d", this.hh, this.mm, this.ss));
			this.reduceASecond();
			try{Thread.sleep(1000);}catch(InterruptedException e){}
		}
	}
}