import java.io.*;
import java.util.*;
public class TimerTest {
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter hh, mm, ss :");
		int hh = Integer.parseInt(br.readLine());
		int mm = Integer.parseInt(br.readLine());
		int ss = Integer.parseInt(br.readLine());
		
		Timer t = new Timer(hh, mm, ss);
		
		
		System.out.println(t.getCurrentTime());
		t.runTimer();
	}
}