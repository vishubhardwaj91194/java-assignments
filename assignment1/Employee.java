import com.google.gson.Gson;
import java.io.*;

public class Employee {
	String name;
	String employeeCode;
	double salary;

	Employee(String n,String e,double s){
		this.name = n;
		this.employeeCode = e;
		this.salary = s;
	}

	public String convertToJSON(){
		Gson gson = new Gson();
 		String json = gson.toJson(this);
		return json;
	}
	public void writeDataTOFile(String fileName, String data) throws IOException {
		File file = new File(fileName);
	      file.createNewFile();
	      FileWriter writer = new FileWriter(file); 
	      writer.write(data); 
	      writer.flush();
	      writer.close();
	}

	public static Employee readDataFromFile(String fileName) throws FileNotFoundException {
		Gson gson = new Gson();
		Employee e = gson.fromJson(new FileReader(fileName), Employee.class);
		return e;
	}

	public void updateSalary(double newSalary) {
		this.salary = newSalary;
	}


}
