import java.io.*;
import java.util.*;


public class PriortyQueueTest {
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of elements : ");
		int n = Integer.parseInt(br.readLine());
		System.out.println("Enter priority elements :");
		
		int arr[] = new int[20];
		int i;
		Integer head;
		
		for(i=0;i<n;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		PriorityQueue < Integer >  prq = new PriorityQueue < Integer > (); 
		for(i = 0; i<n; i++ ){  
			   prq.add (arr[i]) ; 
		}
		
		System.out.println ( "Initial priority queue: "+ prq);
		
		PriorityQueueThread prt = new PriorityQueueThread();
		Thread t;
		
		
		for(i=0;i<n;i++){
			synchronized(prt){
			
				t = new Thread(prt);
				t.start();
			head = prq.poll();
			
			
			System.out.println("Starting task-"+head);
			try{prt.wait();}catch(InterruptedException e){}
			}
		}
	}
}