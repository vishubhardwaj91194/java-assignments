import java.io.*;
import java.util.*;

public class PQT{

     public static void main(String []args) throws IOException{
        System.out.println("Hello World");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of elements : ");
		int n = Integer.parseInt(br.readLine());
		System.out.println("Enter priority elements :");
		
		int arr[] = new int[20];
		int i;
		Task head;
		
		for(i=0;i<n;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		PriorityQueue<Task> prq = new PriorityQueue<Task>(new Comparator<Task>(){
        public int compare(Task l1, Task l2){
            return l1.priority - l2.priority;
        }
    }); 
		for(i = 0; i<n; i++ ){  
			   prq.add (new Task(arr[i])) ; 
		}
		
		System.out.println ( "Initial priority queue: "+ prq);

		
		Thread t;
		
		
		for(i=0;i<n;i++){
			
			    head = prq.poll();
				t = new Thread(head);
				synchronized(head){
				System.out.println("Starting task-"+head.priority);
				t.start();
			
			try{head.wait();}catch(InterruptedException e){}
			}
		}
     }
}

