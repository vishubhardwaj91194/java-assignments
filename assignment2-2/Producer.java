import java.util.*;
public class Producer implements Runnable {
	Data dataObject;
	
	Producer(Data x) {
		this.dataObject = x;
	} 
	
	@Override
	public void run() {
		System.out.println("Producer started.");
		while(this.dataObject.length == 19) {
			synchronized(this.dataObject){
				System.out.println("Overflow, Producer sleeping.");
				try{this.dataObject.wait();}catch(InterruptedException e){}
			}
		}

		
			System.out.println("Producer awake.");
			while(this.dataObject.length != 19){
				synchronized(this){
					int randomNumber = (new Random()).nextInt(101);
					boolean result = this.dataObject.addData(randomNumber);
					this.notifyAll();
					try{Thread.sleep(randomNumber*10);}catch(InterruptedException e){}
				}
			}
			System.out.println("Producer stopping.");				
	}		
}