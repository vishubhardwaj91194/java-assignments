public class Data {
	int data[] = new int[20];
	public int length = 0;
	
	public boolean addData(int number){
		if(length==19) {
			// Overflow
			System.out.println("Overflow.");
			return false;
		} else {
			synchronized(this){
				this.data[length] = number;
				System.out.println(this.data[this.length]+" was added.");
				length++;
				this.notifyAll();
			}

			return true;
		}
	}
	
	public boolean deleteData(){
		if(length==0) {
			// Underflow
			System.out.println("Underflow.");
			return false;
		} else {
			synchronized(this){
				System.out.println(this.data[this.length-1]+" was deleted.");
				length--;
				this.notifyAll();
			}
			return true;
		}
	}
	
	
}