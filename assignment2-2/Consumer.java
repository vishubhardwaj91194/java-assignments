
public class Consumer implements Runnable {
	Data dataObject;
	Producer p;
	Thread ob;
	
	Consumer(Data x, Producer p) {
		this.dataObject = x;
		this.p = p;
	} 
	
	@Override
	public void run() {
		System.out.println("Consumer started.");
		while(this.dataObject.length == 0) {
				synchronized(this.p){
				System.out.println("Underflow, Consumer sleeping.");
				try{this.p.wait();}catch(InterruptedException e){}
			}
		}
		
		// insert Now 
		
			System.out.println("Consumer awake.");
			while(this.dataObject.length !=0){
				synchronized(this.dataObject){
				boolean result = this.dataObject.deleteData();
				try{Thread.sleep(500);}catch(Exception e){}
			}
			}
			System.out.println("Consumer stopping.");				
	}
}